
Our kinetic model for the one equation MHD uses the BGK collision model with a modified equilibrium distribution to include the Maxwell Stress Tensor as second order model;

$$ \frac{\partial f}{\partial t}  + \boldsymbol{\xi}\cdot \frac{\partial f}{\partial \boldsymbol{x}} + \boldsymbol{g}\cdot \frac{\partial f}{\partial \boldsymbol{\xi}} = - \frac{f - f^{eq}}{\tau} $$
The mesoscopic effect of the magnetic field is added to the model through the considering a Lorentz-Force based on the peculiar velocity where $\boldsymbol{g}= -w_c (\boldsymbol{\xi}-\boldsymbol{u})\times\boldsymbol{\hat{b}}$. Where $w_c$ is the cyncotron frequency.

$$ \frac{\partial f}{\partial t}  + \boldsymbol{\xi}\cdot \frac{\partial f}{\partial \boldsymbol{x}} = - \frac{f - f^{eq}}{\tau}+ w_c (\boldsymbol{\xi}-\boldsymbol{u})\times\boldsymbol{\hat{b}}\cdot \frac{\partial f}{\partial \boldsymbol{\xi}}  $$
Before we derive the Lattice-Boltzmann numerical aproximation of the above equation we need to rewrite the force term in the above equation as hermite expantion:
$$ F(\boldsymbol{\xi_o}) = w_c (\xi_\gamma -u_\gamma) b_\nu \varepsilon_{\gamma \nu \eta } \frac{\partial f}{\partial \xi_\eta} = \frac{\omega(\boldsymbol{\xi}_o)}{(RT_o)^{D/2}}  \sum_{n=0}^\infty\frac{1}{n!} a^{(n)}_{r_n} \mathscr{H}^{(n)}_{r_n} (\boldsymbol{\xi}_o)$$
Where $r_n = {\alpha_1 \alpha_2 \dots \alpha_n}$ is the set of $n$ indices of $n$-th rank Hermite polynomial tensor. Due to the ortogonality of the Hermite polynomial is possible to show that
$$  \int w_c (\xi_\gamma -u_\gamma) b_\nu \varepsilon_{\gamma \nu \eta } \frac{\partial f}{\partial \xi_\eta} \mathscr{H}^{(n)}_{r_n} (\boldsymbol{\xi}_o) d^{D}\boldsymbol{\xi} =  a^{(n)}_{r_n} $$
It is possible to show that ([[Expansão da Força]])

$$ a^{(n)}_{r_n} =  - \int f w_c (\xi_\gamma -u_\gamma) b_\nu \varepsilon_{\gamma \nu \eta }  \frac{\partial }{\partial \xi_\eta} \left[   \mathscr{H}^{(n)}_{r_n} (\boldsymbol{\xi}_o)\right] d^{D}\boldsymbol{\xi}  $$
To retrieve a numerical approximation we truncate the above expansion. To obtain the influence of the magnetic field on the transport coefficients up tho the third order is necessary, though only the third order  $\mathscr{H}^{(3)}_{\beta \beta \alpha}$  function is required. If we compute this coefficients we obatin:
$$  a^{(n)}_{r_n}   = 0$$
$$ a^{(1)}_{\alpha_1} = 0$$$$ a^{(2)}_{\alpha_1\alpha_2} =  - \frac{ w_c }{RT_o}  b_\nu   \left[  \tau_{\gamma \alpha_2}\varepsilon_{\gamma \nu \alpha_1 } + \tau_{\gamma \alpha_1} \varepsilon_{\gamma \nu \alpha_2}  \right] $$ $$ a^{(3)}_{\beta\beta\alpha_1}    =  - b_\nu   \frac{w_c}{(RT_o)^{3/2}} \left[  -2u_{\gamma}\varepsilon_{\gamma \nu \eta }   \tau_{\eta \alpha_1}    + 2 q_\gamma \varepsilon_{\gamma \nu \alpha_1 }  + 2u_{\beta} \tau_{\gamma \beta}   \varepsilon_{\gamma \nu \alpha_1 }  \right] $$Assim o termo de força ficará 

$$ F(\boldsymbol{\xi_o}) = \frac{\omega(\boldsymbol{\xi}_o)}{(RT_o)^{D/2}} \left[ \frac{1}{2} a^{(2)}_{\alpha_1 \alpha_2}  \mathscr{H}^{(2)}_{\alpha_1 \alpha_2}(\boldsymbol \xi_o) + \frac{1}{(D-1) 2 +6}  a^{(3)}_{\beta\beta \alpha_1}  \mathscr{H}^{(3)}_{\kappa \kappa\alpha_1}(\boldsymbol \xi_o)  \right]
$$
É importante notar que $\mathscr{H}^{(3)}_{\beta \beta\alpha_1}(\boldsymbol \xi_o)$ não é polinômio tensorial de Hermite, mas sim uma combinação destes. Como este não obedece a relação de normal 
$$ \left\Vert \mathscr{H}^{(3)}_{\beta \beta\alpha_1}(\boldsymbol \xi_o) \mathscr{H}^{(3)}_{\eta \eta\alpha_1}(\boldsymbol \xi_o) \right\Vert= \left\Vert \left[  \mathscr{H}^{(3)}_{11\alpha_1}(\boldsymbol \xi_o) + \mathscr{H}^{(3)}_{22\alpha_1}(\boldsymbol \xi_o)  + \dots \mathscr{H}^{(3)}_{DD\alpha_1}(\boldsymbol \xi_o)  \right]\left[  \mathscr{H}^{(3)}_{11\alpha_1}(\boldsymbol \xi_o) + \mathscr{H}^{(3)}_{22\alpha_1}(\boldsymbol \xi_o)  + \dots \mathscr{H}^{(3)}_{DD\alpha_1}(\boldsymbol \xi_o)  \right] \right\Vert = \left\Vert  \mathscr{H}^{(3)}_{11\alpha_1}(\boldsymbol \xi_o)² \right\Vert+ \left\Vert  \mathscr{H}^{(3)}_{22\alpha_1}(\boldsymbol \xi_o)^2 \right\Vert + \dots\left\Vert  \mathscr{H}^{(3)}_{DD\alpha_1}(\boldsymbol \xi_o)^2 \right\Vert= (D-1) 2! + 3!$$
Uma forma anâloga de obter o termo de terceira ordem é utilizar ortogonalização de Grann-Schmidt para obter a correta descrição do momento de $\xi^2 \xi_\alpha$ sem afetar os momentos de baixa ordens já descritos pelos polinômios com ordem inferior a dois.

$$ F(\boldsymbol{\xi_o}) = \frac{\omega(\boldsymbol{\xi}_o)}{(RT_o)^{D/2}} \left\{ \frac{-1}{2} \frac{ w_c }{RT_o}  b_\nu   \left[  \tau_{\gamma \alpha_2}\varepsilon_{\gamma \nu \alpha_1 } + \tau_{\gamma \alpha_1} \varepsilon_{\gamma \nu \alpha_2}  \right] \left[  \xi_{o,\alpha_1} \xi_{o,\alpha_2} - \delta_{\alpha_1\alpha_2}  \right] -\frac{  b_\nu }{10}    \frac{w_c}{(RT_o)^{3/2}} \left[  -2u_{\gamma}\varepsilon_{\gamma \nu \eta }   \tau_{\eta \alpha_1}    + 2 q_\gamma \varepsilon_{\gamma \nu \alpha_1 }  + 2u_{\beta} \tau_{\gamma \beta}   \varepsilon_{\gamma \nu \alpha_1 }  \right]
  \left[  \xi_{o,\kappa} \xi_{o,\kappa}  \xi_{o,\alpha_1} - 5\xi_{o,\alpha_1}  \right] \right\}
$$
Onde assumimos que $D=3$ 
$$ F(\boldsymbol{\xi_o}) = \frac{\omega(\boldsymbol{\xi}_o)}{(RT_o)^{D/2}} \left\{ \frac{-1}{2} \frac{ w_c }{RT_o}  b_\nu   \left[  2 \tau_{\gamma \alpha_2}\varepsilon_{\gamma \nu \alpha_1 }\xi_{o,\alpha_1} \xi_{o,\alpha_2} \right] -\frac{  b_\nu }{10}    \frac{w_c}{(RT_o)^{3/2}} \left[  -2u_{\gamma}\varepsilon_{\gamma \nu \eta }   \tau_{\eta \alpha_1}  (\xi_{o,\kappa} \xi_{o,\kappa}  \xi_{o,\alpha_1} - 5\xi_{o,\alpha_1} )   + 2 q_\gamma \varepsilon_{\gamma \nu \alpha_1 }  (\xi_{o,\kappa} \xi_{o,\kappa}  \xi_{o,\alpha_1} - 5\xi_{o,\alpha_1} ) + 2u_{\beta} \tau_{\gamma \beta}   \varepsilon_{\gamma \nu \alpha_1 }  (\xi_{o,\kappa} \xi_{o,\kappa}  \xi_{o,\alpha_1} - 5\xi_{o,\alpha_1} ) \right]
 \right\}
$$

$$ F(\boldsymbol{\xi_o}) = \frac{\omega(\boldsymbol{\xi}_o)}{(RT_o)^{D/2}} w_c    \left\{   \left[(  \boldsymbol{\xi_o}\times \hat{\boldsymbol{b}}) \cdot ( \boldsymbol{\overline{\overline{\tau_o}}}\cdot \boldsymbol{\xi_o} )\right] +\frac{ 2 }{10}    \left[  (\boldsymbol{u_o} \times  \hat{\boldsymbol{b}} )\cdot \boldsymbol{\overline{\overline{\tau_o}}}   -  (\boldsymbol{q_o}\times  \hat{\boldsymbol{b}} )-  (\boldsymbol{u_o}\cdot \boldsymbol{\overline{\overline{\tau_o}}} ) \times  \hat{\boldsymbol{b}} \right] \cdot ({\xi_o}^2 \boldsymbol{\xi}_o - 5\boldsymbol{\xi}_o ) 
 \right\}
$$
Definindo:
$$ \phi_i \equiv \frac{\omega_i}{\omega(\boldsymbol\xi_{o,i})} (RT_0)^{D/2} \phi(\boldsymbol\xi_{o,i}) $$
Temo também que
$$ \begin{align} \overline{\overline{\boldsymbol\sigma}}_{o} & = (RT_o)^{D/2} \int  (f - f_{eq}) (\boldsymbol \xi_o - \boldsymbol u_o)(\boldsymbol \xi_o - \boldsymbol u_o) d \boldsymbol{\xi_o} =\int  \omega(\boldsymbol\xi_{o})\frac{ (RT_o)^{D/2} (f - f_{eq})}{\omega(\boldsymbol\xi_{o})} (\boldsymbol \xi_o - \boldsymbol u_o)(\boldsymbol \xi_o - \boldsymbol u_o) d \boldsymbol{\xi_o} \\
 & = \sum_i  w_i  \frac{ (RT_o)^{D/2} (f - f_{eq})}{\omega(\boldsymbol\xi_{o,i})} (\boldsymbol \xi_{o,i} - \boldsymbol u_{o,i}) (\boldsymbol \xi_{o,i} - \boldsymbol u_{o,i}) =  \sum_i  (f_i - f^{eq}_i)  (\boldsymbol \xi_{o,i} - \boldsymbol u_{o,i}) (\boldsymbol \xi_{o,i} - \boldsymbol u_{o,i})
\end{align} $$
Para fins de facilitara implementação representa-se as quantidades em unidades de rede $\boldsymbol{\xi}_{o,i} = a \boldsymbol{c}_i$ onde $\boldsymbol{c}_i$ tem somente componentes inteiras e $a$ é um fator de escala.

$$ \overline{\overline{\boldsymbol\sigma}}_{o}  =  a^2 \sum_i  (f_i - f^{eq}_i)  (\boldsymbol c_{i} - \boldsymbol u)  (\boldsymbol c_{i} - \boldsymbol u) =  a² \overline{\overline{\boldsymbol\sigma}} $$
De modo análogo
$$ \begin{align} {{\boldsymbol q}}_{o}&= \int  \omega(\boldsymbol\xi_{o})\frac{ (RT_o)^{D/2} f}{\omega(\boldsymbol\xi_{o})}f ( \xi_o -  u_o)^2(\boldsymbol \xi_o - \boldsymbol u_o)d \boldsymbol{\xi_o} \\
 &  = \sum_i  f_i ( \xi_{o,i} - u_{o,i})^2 (\boldsymbol \xi_{o,i} - \boldsymbol u_{o,i}) =  a^3 \sum_i  f_i ( c_{i} - u_{i})^2 (\boldsymbol c_{i} - \boldsymbol u_{i}) = a^3 \boldsymbol{q}
\end{align} $$
Assim a versão discreta do termo de força fica
$$ F_i = w_c \omega_i     \left\{   a^4 \left[(  \boldsymbol{c_i}\times \hat{\boldsymbol{b}}) \cdot ( \boldsymbol{\overline{\overline{\sigma}}}\cdot \boldsymbol{c_i} )\right] +\frac{ 2 }{10}  a^6  \left[   (\boldsymbol{u} \times  \hat{\boldsymbol{b}} )\cdot \boldsymbol{\overline{\overline{\sigma}}}   -  (\boldsymbol{q}\times  \hat{\boldsymbol{b}} )-  (\boldsymbol{u}\cdot \boldsymbol{\overline{\overline{\sigma}}} ) \times  \hat{\boldsymbol{b}} \right] \cdot ({c_i}^2 \boldsymbol{c}_i - \frac{5}{a²}  \boldsymbol{c}_i ) 
	 \right\}
$$
Em LBM utiliza-se, em geral, a0.tização semi-implícita devido ao aumento em uma ordem na precisão do método sem grande implicações na eficiência computacional.  Porém como o termo de força proposta necessita do cálculo de momentos de alta ordem, é necessário alguns cuidados adicionais. Seja a equação LBM-BGK na forma semi-implicita:
$$ \frac{\tilde{f}_i\left(\boldsymbol{x}+\boldsymbol{c_i} \delta, t+\delta\right)-\tilde{f}_i(\boldsymbol{x}, t)}{\delta}=-\frac{\tilde{f}_i(x, t)-\tilde{f}_i^{eq}(\boldsymbol{x}, t)}{\tilde{\tau} } +\frac{(\tilde{\tau}-\delta/2)}{\tilde{\tau}} F_i $$
Aqui $\tilde \tau = \tau + \delta/2$, onde $\tau$ é o tempo de relaxação da equação Boltzmann-BGK e que a função distribuição modificada $\tilde f_i$ é definida como
$$ \tilde{f}_i = f_i - \frac{\delta}{2}  \Omega_i  - \frac{\delta}{2}F_i  $$
Calculando os primeiros momentos temos 
$$  \sum_i \tilde{f}_i = \sum_i  f_i = \rho $$
$$  \sum_i \tilde{f}_i \boldsymbol{c}_i = \sum_i  f_i  \boldsymbol{c}_i = \rho \boldsymbol u $$
$$  \sum_i \tilde{f}_i c_i^2 = \sum_i  f_i c_i^2 = 2 \rho u^2 + \frac{1}{a^2}D\rho \theta$$

 <span style="color:red"> *É preciso mudar para incluir o campo magnético que será adicionado no segundo momento da função distribuição de equilíbrio</span>
 
Onde $\theta = T/T_o$.  Logo os momentos conservados pelo operador de colisão e pelo termo de força pode ser calculados diretamente da distribuição modificada. Porém isso não ocorre para o tensor tensão e o fluxo do calor. :

$$ \begin{align} \sum (\tilde{f}_i - \tilde{f}^{eq}_i) (c_i - u_i)² (c_{i,\alpha} - u_{\alpha}) = &  \sum (f_i-f_i^{eq}) (c_i - u_i)² (c_{i,\alpha} - u_{\alpha})   + \frac{\delta}{2\tau}\sum  (f_i - f_i^{eq}) (c_i - u_i)² (c_{i,\alpha} - u_{\alpha}) -   \frac{\delta}{2}  \sum F_i (c_i - u_i)² (c_{i,\alpha} - u_{\alpha}) \\ = &  2 q_{\alpha}   + \frac{\delta}{2\tau} 2 q_{\alpha} -   \frac{\delta}{2}  \sum F_i (c_i - u_i)² (c_{i,\alpha} - u_{\alpha})  \end{align} $$

Temos que 
$$ \begin{align} \sum F_i (c_i - u_i)² (c_{i,\alpha} - u_{\alpha}) & = (RT_0)^{D/2} \frac{1}{a^3} \int F(\boldsymbol \xi_o)  (\xi_{o} - u_o)² (\xi_{o,\alpha} - u_{o,\alpha})  d\boldsymbol \xi_o \\ & =  \frac{1}{a^3} \int F(\boldsymbol \xi_o)  (\xi_{o} - u_o)² (\xi_{o,\alpha} - u_{o,\alpha})  d\boldsymbol \xi \\ 
  & =  -  2 \frac{1}{a^3}  w_c q_{o,\gamma} b_\nu \epsilon_{\gamma \nu \alpha}=   -   2 w_c  q_{\gamma} b_\nu \epsilon_{\gamma \nu \alpha}\end{align} $$
Assim,
$$ \begin{align} \sum \tilde{f}_i  (c_i - u_i)² (c_{i,\alpha} - u_{\alpha}) = &  2 q_{\alpha}   + \frac{\delta}{2\tau} 2q_{\alpha} +   \frac{\delta}{2}  w_c  2 q_{\gamma} b_\nu \epsilon_{\gamma \nu \alpha}  \end{align} $$
Definindo 

$$2\tilde q_\alpha \equiv \sum_i \tilde{f}_i  (c_i - u_i)² (c_{i,\alpha} - u_{\alpha})$$
Temos o seguinte sistema para cálculo de $q_\alpha$
$$   \left[ \delta_{\gamma \alpha} \left( 1 + \frac{\delta}{2\tau}\right)    +  \frac{\delta}{2} \omega_c  \hat{b}_\beta\epsilon_{\gamma  \beta \alpha} \right] q_\gamma   =  \tilde q_\alpha  $$
Podemos definir a matriz 
$$ Q_{\alpha \gamma} = \left[ \delta_{\gamma \alpha} \left( 1 + \frac{\delta}{2\tau}\right)    +  \frac{\delta}{2} \omega_c  \hat{b}_\beta\epsilon_{\gamma  \beta \alpha} \right]$$
Que tem a forma,
$$ \boldsymbol Q = \left[\begin{matrix}\frac{\delta}{2 \tau} + 1 & \frac{\delta \omega_{c} {b}_{3}}{2} & - \frac{\delta \omega_{c} {b}_{2}}{2}\\- \frac{\delta \omega_{c} {b}_{3}}{2} & \frac{\delta}{2 \tau} + 1 & \frac{\delta \omega_{c} {b}_{1}}{2}\\\frac{\delta \omega_{c} {b}_{2}}{2} & - \frac{\delta \omega_{c} {b}_{1}}{2} & \frac{\delta}{2 \tau} + 1\end{matrix}\right] =  \left( \frac{\delta}{2 \tau} + 1 \right)\boldsymbol I + \frac{\delta}{2} \omega_c \boldsymbol X$$
Onde $$ X_{\alpha \gamma} = \hat{b}_\beta\epsilon_{\gamma  \beta \alpha}  $$Assim,
$$  \boldsymbol Q  \boldsymbol{q} = \boldsymbol{\tilde  q} $$
A matriz $Q$ pode ser analiticamente invertida para fornecer 	a
$$\begin{align} \boldsymbol Q^{-1} & = \frac{\tau}{\delta^{3} \omega_{c}^{2} \tau^{2} + \delta^{3} + 2 \delta^{2} \omega_{c}^{2} \tau^{3}  + 6 \delta^{2} \tau + 12 \delta \tau^{2} + 8 \tau^{3} }\left[\begin{matrix}2 \delta^{2} \omega_{c}^{2} \tau^{2} {b}_{1}^{2} + 2 \delta^{2} + 8 \delta \tau + 8 \tau^{2} & 2 \delta \omega_{c} \tau \left(\delta \omega_{c} \tau {b}_{1} {b}_{2} - \delta {b}_{3} - 2 \tau {b}_{3}\right) & 2 \delta \omega_{c} \tau \left(\delta \omega_{c} \tau {b}_{1} {b}_{3} + \delta {b}_{2} + 2 \tau {b}_{2}\right)\\2 \delta \omega_{c} \tau \left(\delta \omega_{c} \tau {b}_{1} {b}_{2} + \delta {b}_{3} + 2 \tau {b}_{3}\right) & 2 \delta^{2} \omega_{c}^{2} \tau^{2} {b}_{2}^{2} + 2 \delta^{2} + 8 \delta \tau + 8 \tau^{2} & 2 \delta \omega_{c} \tau \left(\delta \omega_{c} \tau {b}_{2} {b}_{3} - \delta {b}_{1} - 2 \tau {b}_{1}\right)\\2 \delta \omega_{c} \tau \left(\delta \omega_{c} \tau {b}_{1} {b}_{3} - \delta {b}_{2} - 2 \tau {b}_{2}\right) & 2 \delta \omega_{c} \tau \left(\delta \omega_{c} \tau {b}_{2} {b}_{3} + \delta {b}_{1} + 2 \tau {b}_{1}\right) & 2 \delta^{2} \omega_{c}^{2} \tau^{2} {b}_{3}^{2} + 2 \delta^{2} + 8 \delta \tau + 8 \tau^{2}\end{matrix}\right] \\  & = \frac{\tau \left[2 \delta^{2} \omega_{c}^{2} \tau^{2} \boldsymbol{\hat b} \boldsymbol{\hat b} + \left(2 \delta^{2} + 8 \delta \tau + 8 \tau^{2}\right) \boldsymbol{I} -  2 \delta \omega_{c} \tau  (\delta  + 2 \tau) \boldsymbol X\right]}{\delta^{3} \omega_{c}^{2} \tau^{2} + \delta^{3} + 2 \delta^{2} \omega_{c}^{2} \tau^{3}  + 6 \delta^{2} \tau + 12 \delta \tau^{2} + 8 \tau^{3} } \end{align}  $$
Assim, podemos calcular $\boldsymbol{q}$ como 
$$   \boldsymbol{q} = \frac{\tau \left[2 \delta^{2} \omega_{c}^{2} \tau^{2} \boldsymbol{\hat b} (\boldsymbol{\hat b}\cdot \boldsymbol{\tilde  q} ) + \left(2 \delta^{2} + 8 \delta \tau + 8 \tau^{2}\right) \boldsymbol{\tilde  q} -  2 \delta \omega_{c} \tau  (\delta  + 2 \tau) (\boldsymbol{\tilde q} \times \hat{\boldsymbol{b}}) \right]}{\delta^{3} \omega_{c}^{2} \tau^{2} + \delta^{3} + 2 \delta^{2} \omega_{c}^{2} \tau^{3}  + 6 \delta^{2} \tau + 12 \delta \tau^{2} + 8 \tau^{3} }$$
Note que  $X_{\alpha \gamma} \tilde q_\gamma = \hat{b}_\beta\epsilon_{\gamma  \beta \alpha}  \tilde q_\gamma =  (\boldsymbol{\tilde q} \times \hat{\boldsymbol{b}})_\alpha$

Por fim, temos que determinar o tensor tensão viscosa,
$$ \sum_i (\tilde{f}_i - f_i^{eq})(c_{i,\alpha} - u_\alpha)(c_{i,\beta} - u_\beta)= \sum_i (f_i  - f_i^{eq})(c_{i,\alpha} - u_\alpha)(c_{i,\beta} - u_\beta) - \frac{\delta}{2} \sum_i  \Omega_i (c_{i,\alpha} - u_\alpha)(c_{i,\beta} - u_\beta) - \frac{\delta}{2} \sum_i  F_i(c_{i,\alpha} - u_\alpha)(c_{i,\beta} - u_\beta)  $$

$$ \sum_i (\tilde{f}_i - f_i^{eq})(c_{i,\alpha} - u_\alpha)(c_{i,\beta} - u_\beta)= \sum_i (f_i  - f_i^{eq})(c_{i,\alpha} - u_\alpha)(c_{i,\beta} - u_\beta) + \frac{\delta}{2\tau} \sum_i  (f_i - f_i^{eq}) (c_{i,\alpha} - u_\alpha)(c_{i,\beta} - u_\beta) - \frac{\delta}{2} \sum_i  F_i(c_{i,\alpha} - u_\alpha)(c_{i,\beta} - u_\beta)  $$
Usando definição $\tau_{\alpha \beta}$ e 

$$\sum_i  F_i(c_{i,\alpha} - u_\alpha)(c_{i,\beta} - u_\beta)= \frac{1}{a²}\int F (\xi_{o,\alpha} - u_{o,\alpha})(\xi_{o,\beta} - u_{o,\beta}) d^{D} \boldsymbol \xi=   -  w_c \left[ b_\nu  \sigma_{\gamma \beta}\varepsilon_{\gamma \nu \alpha } + b_\nu \sigma_{\gamma \alpha} \varepsilon_{\gamma \nu \beta}  \right] $$

E empregando a definição $\tilde \sigma_{\alpha \beta} \equiv \sum_i (\tilde{f}_i - f_i^{eq})(c_{i,\alpha} - u_\alpha)(c_{i,\beta} - u_\beta)$. Assim,

	$$  \tilde \sigma_{\alpha \beta}  = \left(1 + \frac{\delta}{2\tau} \right) \sigma_{\alpha\beta} +\frac{\delta}{2}   w_c b_\nu\left[  \varepsilon_{\gamma \nu \alpha }\sigma_{\gamma\beta} +  \varepsilon_{\gamma \nu \beta} \sigma_{\gamma\alpha} \right]   $$
		$$  \frac{2\tau}{\left(2\tau + \delta \right)}\tilde \sigma_{\alpha \beta}  =  \sigma_{\alpha\beta} +  \frac{2\tau}{\left(2\tau + \delta \right)}\frac{ \delta w_c}{2} b_\nu\left[  \varepsilon_{\gamma \nu \alpha }\sigma_{\gamma\beta} +  \varepsilon_{\gamma \nu \beta} \sigma_{\gamma\alpha} \right]   $$
$$  \frac{2\tau}{\left(2\tau + \delta \right)}\tilde \sigma_{\alpha \beta}  =  \sigma_{\alpha\beta} + \lambda  b_\nu\left[  \varepsilon_{\gamma \nu \alpha }\sigma_{\gamma\beta} +  \varepsilon_{\gamma \nu \beta} \sigma_{\gamma\alpha} \right]   $$

Onde $\lambda_1 = \frac{2\tau}{\left(2\tau + \delta \right)}=\frac{2\tilde \tau - \delta}{2\tilde\tau}$  e  $\lambda_2 = \lambda_1 \frac{ \delta w_c}{2}$

$$  \lambda_1 \tilde \sigma_{\alpha \beta}  =  \sigma_{\alpha\beta} + \lambda_2  b_\nu\left[  \varepsilon_{\gamma \nu \alpha }\sigma_{\gamma\beta} +  \varepsilon_{\gamma \nu \beta} \sigma_{\gamma\alpha} \right]   $$






$$ \sum_i f_i (\xi_{o,i} - u_{o,i})^2 = a^2 \sum_i f_i (c_i - u_i)^2 = D \rho \theta$$


$$ 2q= \sum f_i c_i^2 c_{i,\alpha}-2u_\beta\sum f_i c_{i,\beta} c_{i,\alpha}+  u^2 \rho u_\alpha  - u_\alpha \sum f_i (c_i^2 -c_{i,\beta}u_\beta+ u^2)   $$$$ \omega_c = -e B/m$$





 $$ \sum F_i  (c_i^2 c_{i,\alpha} - 2 c_{i,\beta} u_{i,\beta} c_{i,\alpha}  - u_i^2 c_{i,\alpha})  ) $$
Definição da energia interna:

$$ \Pi_{\alpha \beta} = \int (f - f^{eq}) \xi_\alpha \xi_\beta d\boldsymbol\xi $$
Assim,
$$ \Pi_{\alpha \beta} = \int f  \xi_\alpha \xi_\beta d\boldsymbol\xi  -  \int f^{eq}\xi_\alpha \xi_\beta d\boldsymbol\xi = \int f  \xi_\alpha \xi_\beta d\boldsymbol\xi - \rho u_\alpha u_\beta - P_{\alpha \beta} - M_{\alpha \beta} $$
Este pode ser reescrito como:
$$ \begin{align}\Pi_{\alpha \beta} & = \int f  \xi_\alpha \xi_\beta d\boldsymbol\xi - u_\beta\int f  \xi_\alpha d\boldsymbol\xi  - P_{\alpha \beta} - M_{\alpha \beta}\\ & = \int f  \xi_\alpha (\xi_\beta-u_\beta) d\boldsymbol\xi   - P_{\alpha \beta} - M_{\alpha \beta}\\ & = \int f  (\xi_\alpha - u_\alpha + u_\alpha) (\xi_\beta-u_\beta) d\boldsymbol\xi   - P_{\alpha \beta}- M_{\alpha \beta} \\ & = \int f  (\xi_\alpha - u_\alpha) (\xi_\beta-u_\beta) d\boldsymbol\xi + u_\alpha\int f  (\xi_\beta-u_\beta) d\boldsymbol\xi  - P_{\alpha \beta} - M_{\alpha \beta}\\ & = \int f  (\xi_\alpha - u_\alpha) (\xi_\beta-u_\beta) d\boldsymbol\xi  - P_{\alpha \beta} - M_{\alpha \beta} \end{align} $$

Definimos a energia interna como:
$$ \begin{align} 2 \rho e &= \int f (\xi_\alpha - u_\alpha) (\xi_\alpha - u_\alpha)d \boldsymbol{\xi} - M_{\alpha \alpha} \\ & = \int f \xi_\alpha \xi_\alpha d \boldsymbol{\xi} -\rho u_\alpha u_\alpha   - M_{\alpha \alpha} \end{align} $$
Já o fluxo de calor é definido como:
	$$ \begin{align} 2q_{\alpha} = \int (f - f^{eq})  c_\beta  c_\beta c_\alpha d\boldsymbol c = \int f   c_\beta  c_\beta c_\alpha d\boldsymbol c -   \int f^{mag} c_\beta  c_\beta c_\alpha d\boldsymbol c= \int f   c_\beta  c_\beta c_\alpha d\boldsymbol c  + \frac{D {B}^2 {u}_{\alpha}}{2} - 2 (\boldsymbol{B} \cdot \boldsymbol{u} ) {u}_{\alpha} \\
\end{align}   $$






Assim,

$$  2 q_\alpha = \int f  (\xi_\beta - u_\beta)  (\xi_\beta - u_\beta) (\xi_\alpha -u_\alpha) d\boldsymbol\xi  + M_{\beta \beta}  u_\alpha  +  2 u_\beta  M_{\alpha \beta}  $$



$$ \boldsymbol{F}=(\boldsymbol{\nabla} \times\boldsymbol{B} )  \times \boldsymbol{B}$$

$$ a_{r_1} = \biggr\langle \mathscr{H}_{r_1}(c)\boldsymbol{g}\cdot \frac{\partial f}{\partial \boldsymbol{\xi}} \biggr\rangle  $$

$$ \boldsymbol{g}\cdot \frac{\partial f}{\partial \boldsymbol{\xi}} = \omega\boldsymbol(c) \frac{1}{n!} \sum_{n=0}^\infty a_{r_n} \mathscr{H}_{r_n}(c) $$

$$ r_n = {\alpha_1 \alpha_2 \dots \alpha_n}$$



$$ \boldsymbol{g}\cdot \frac{\partial f^{eq}}{\partial \boldsymbol{c}}  = - \boldsymbol{g}\cdot\boldsymbol{c}f^{eq}     $$$$ \lambda_0\frac{1}{1 + \omega_c^2 (\tilde\tau-1/2)^2}$$

$$ \tilde \tau= \frac{\tau}{\delta} $$$$\left[\begin{matrix}\frac{2 \alpha_{c}^{2} + 1}{4 \alpha_{c}^{2} + 1} & - \frac{2 \alpha_{c}}{4 \alpha_{c}^{2} + 1} & \frac{2 \alpha_{c}^{2}}{4 \alpha_{c}^{2} + 1} & 0 & 0\\\frac{\alpha_{c}}{4 \alpha_{c}^{2} + 1} & \frac{1}{4 \alpha_{c}^{2} + 1} & - \frac{\alpha_{c}}{4 \alpha_{c}^{2} + 1} & 0 & 0\\\frac{2 \alpha_{c}^{2}}{4 \alpha_{c}^{2} + 1} & \frac{2 \alpha_{c}}{4 \alpha_{c}^{2} + 1} & \frac{2 \alpha_{c}^{2} + 1}{4 \alpha_{c}^{2} + 1} & 0 & 0\\0 & 0 & 0 & \frac{1}{\alpha_{c}^{2} + 1} & \frac{\alpha_{c}}{\alpha_{c}^{2} + 1}\\0 & 0 & 0 & - \frac{\alpha_{c}}{\alpha_{c}^{2} + 1} & \frac{1}{\alpha_{c}^{2} + 1}\end{matrix}\right]$$




$$ {m}_{5} = \frac{2 \beta_{c}^{2} \lambda {m}_{8}}{4 \beta_{c}^{2} + 1} - \frac{2 \beta_{c} \lambda {m}_{6}}{4 \beta_{c}^{2} + 1} + \frac{\left(2 \beta_{c}^{2} \lambda + \lambda\right) {m}_{5}}{4 \beta_{c}^{2} + 1} $$
$$ {m}_{6} = \frac{\beta_{c} \lambda {m}_{5}}{4 \beta_{c}^{2} + 1} - \frac{\beta_{c} \lambda {m}_{8}}{4 \beta_{c}^{2} + 1} + \frac{\lambda {m}_{6}}{4 \beta_{c}^{2} + 1} $$
$$ {m}_{8} = \frac{2 \beta_{c}^{2} \lambda {m}_{5}}{4 \beta_{c}^{2} + 1} + \frac{2 \beta_{c} \lambda {m}_{6}}{4 \beta_{c}^{2} + 1} + \frac{\left(2 \beta_{c}^{2} \lambda + \lambda\right) {m}_{8}}{4 \beta_{c}^{2} + 1} $$
$$ {m}_{9} = \frac{\beta_{c} {m}_{7}}{\frac{\beta_{c}^{2}}{\lambda} + \frac{1}{\lambda}} + \frac{{m}_{9}}{\frac{\beta_{c}^{2}}{\lambda} + \frac{1}{\lambda}} $$
$$ {m}_{7} = - \frac{\beta_{c} \lambda {m}_{9}}{\beta_{c}^{2} + 1} + \frac{{m}_{7}}{\frac{\beta_{c}^{2}}{\lambda} + \frac{1}{\lambda}} $$