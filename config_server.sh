#!/bin/bash

URL=$1
REMOTEUSER=$2
SERVER=$(echo $URL | cut -d'.' -f1)
LOCALUSER=$(id -u -n)

echo "URL: ${URL}"
echo "NAME: ${SERVER}"
echo "REMOTE USER: ${REMOTEUSER}"
echo "LOCAL USER: ${LOCALUSER}"

LOG=server_config.sh

touch $LOG

function addHost {
	echo -e "\nHost $1"
	echo -e "\tUser $2"
	echo -e "\tHostName $3"
	echo -e "\tPort 22"
}

SSHCONFIG=$HOME/.ssh/config

if [ ! -d  $HOME/.ssh ]; then
	mkdir $HOME/.ssh
fi

if [ -a $SSHCONFIG ]; then
	if grep -q $SERVER "$SSHCONFIG"; then
		echo -e "Configurando apelido para a máquina $SERVER: [Já existe]"
	else
		addHost $SERVER $REMOTEUSER $URL >> $SSHCONFIG
		echo -e "Configurando apelido para a máquina $SERVER: [OK]"
	fi
else
	addHost $SERVER $REMOTEUSER $URL >> $SSHCONFIG
	echo -e "Configurando apelido para a máquina $SERVER: [OK]"
fi

if [ ! -a $HOME/.ssh/id_rsa.pub ]; then
	cat /dev/zero | ssh-keygen -q -N "" > /dev/null
	echo -e "\nCriando chave para acesso sem senha [OK]"
fi

ssh-copy-id $SERVER >> $LOG

echo -e "\nConfigurando montagem das máquinas remotas"

if [ -d $HOME/remote ]; then
	echo -e "\tCriando pasta $HOME/remote [Já existe]"
else
	mkdir $HOME/remote
	echo -e "\tCriando pasta $HOME/remote [OK]"
fi

GID=$(id -g)

if [ -d $HOME/remote/$SERVER ]; then
	echo -e "\tCriando pasta $HOME/remote/$SERVER [Já existe]"
else
	mkdir $HOME/remote/$SERVER
	echo -e "\tCriando pasta $HOME/remote/$SERVER [OK]"
fi

if grep -q sshfs#$SERVER /etc/fstab; then
	echo "Configurando $SERVER em /etc/fstab [Já Existe]"
else
	echo "sshfs#${SERVER}:/home/${REMOTEUSER} ${HOME}/remote/${SERVER}  fuse      user,_netdev,reconnect,uid=$UID,gid=$GID,idmap=user,allow_other  0   0"  | sudo tee -a /etc/fstab > /dev/null
	echo "Configurando $SERVER em /etc/fstab [OK]"
fi

sudo sed -i 's/#user_allow_other/user_allow_other/g' /etc/fuse.conf
sudo systemctl daemon-reload

